from MDPs import mdp
import torch
import numpy as np
from methods import value_iteration, policy_iteration

torch.manual_seed(0)

eps_dev = 100
gamma = 0.4

mdp = mdp(500, 10, gamma)

V_opt = mdp.V_opt()

lower_bound_GC = (1+gamma)/2
lower_bound_LC = max(lower_bound_GC, 1/(1+gamma))

alpha = [0.6, 0.7, 0.8, 0.9, 1.0, 1.1]

res_alpha_VI = []

for alpha_i in alpha:
    res_alpha_VI.append(value_iteration(mdp, alpha_i, V_opt, tol=10**-13, eps = eps_dev))

pi_0 = torch.randint(0, mdp.action_size, (mdp.state_size, ))

res_PI = policy_iteration(mdp, pi_0, V_opt, tol=10**-13, eps = eps_dev)

import matplotlib.pyplot as plt
plt.rcParams.update({
"text.usetex": True,
"font.family": "sans-serif",
"font.sans-serif": ["Times"]})
# for Palatino and other serif fonts use:
plt.rcParams.update({
    "text.usetex": True,
    "font.family": "serif",
    'font.size': 15,
    "font.serif": ["Palatino"],
})
# It's also possible to use the reduced notation by directly setting font.family:
plt.rcParams.update({
"text.usetex": True,
"font.family": "Helvetica"
})

plt.figure()

for ii, alpha_i in enumerate(alpha):
    if alpha_i != 1:
        plt.semilogy(res_alpha_VI[ii], label="{:.2f}-VI".format(alpha_i))
    else:
        plt.semilogy(res_alpha_VI[ii], label="1.0-VI = VI")

plt.semilogy(res_PI, label="PI", color='k')

plt.xlabel("iterations")
plt.ylabel(r"$\Vert \theta_k - \theta^* \Vert_{\infty}$")
plt.legend()
plt.grid()
plt.ylim(10**-13, 5*10**2)
plt.show()  

