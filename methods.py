from MDPs import mdp
import torch

        
def value_iteration(mdp, alpha, V_opt, seed=0, iterations=10**4, tol=10**-9, eps=None):
    
    torch.manual_seed(seed)

    if eps is not None:
    	V = V_opt + eps*torch.ones((mdp.state_size, ), dtype=torch.float64)
    else:
    	V = torch.randn((mdp.state_size, ), dtype=torch.float64)
    
    res = [torch.max(torch.abs(V-V_opt)).item()]

    for iter in range(iterations):

        V = (-(1-alpha)/alpha)*V + (1/alpha)*torch.max((mdp.r + mdp.gamma*torch.matmul(mdp.P, V)), 0)[0]
        
        res.append(torch.max(torch.abs(V-V_opt)).item())
        
        if torch.max(torch.abs(V-V_opt)).item()<=tol:
            break
    
    return res
    
def policy_iteration(mdp, pi, V_opt, seed=0, iterations=10**2, tol=10**-9, eps=None):

    torch.manual_seed(seed)
    
    I = torch.eye(mdp.state_size)
    states_idx = torch.arange(mdp.state_size)

    if eps is not None:
    	V = V_opt + eps*torch.ones((mdp.state_size, ), dtype=torch.float64)
    else:
    	V = torch.randn((mdp.state_size, ), dtype=torch.float64)

    res = [torch.max(torch.abs(V-V_opt)).item()]

    for iter in range(iterations):

        P = mdp.P[pi, states_idx, :]
        
        #policy evaluation
        V = torch.matmul(torch.inverse(I-mdp.gamma*P), mdp.r[pi, states_idx])
        #policy improvement
        pi = torch.argmax(mdp.r + mdp.gamma * torch.matmul(mdp.P, V), 0)
        
        res.append(torch.max(torch.abs(V-V_opt)).item())
        
        if torch.max(torch.abs(V-V_opt)).item()<=tol:
            break
    
    return res