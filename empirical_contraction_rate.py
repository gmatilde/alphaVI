from MDPs import mdp
import numpy as np
from methods import value_iteration
    
eps_dev = 100
gamma = 0.4

mdp = mdp(500, 10, gamma, seed=0)

V_opt = mdp.V_opt()

steep = []

#computing the bounds from the theory
lower_bound_GC = (1+gamma)/2
lower_bound_LC = max(lower_bound_GC, 1/(1+gamma))

alpha = np.linspace(0.1, 1.5, num=1000).tolist()

res_alpha_VI = []

for alpha_i in alpha:
    res_alpha_VI.append(value_iteration(mdp, alpha_i, V_opt, eps = eps_dev))
    steep.append(((res_alpha_VI[-1][1])/res_alpha_VI[-1][0]+ (res_alpha_VI[-1][2])/res_alpha_VI[-1][1] + (res_alpha_VI[-1][3])/res_alpha_VI[-1][2])/3)

alpha = np.asarray(alpha)
steep = np.asarray(steep)

segment1 = (alpha>= lower_bound_LC) & (alpha<1)
segment2 = (alpha< lower_bound_LC) 
segment3 = (alpha>=1)
segment4 = (alpha>lower_bound_GC)

import matplotlib.pyplot as plt
plt.rcParams.update({
"text.usetex": True,
"font.family": "sans-serif",
"font.sans-serif": ["Times"]})

plt.rcParams.update({
    "text.usetex": True,
    "font.family": "serif",
    'font.size': 15,
    "font.serif": ["Palatino"],
})

plt.rcParams.update({
"text.usetex": True,
"font.family": "Helvetica"
})

plt.figure()

    
plt.semilogy(alpha[segment1], steep[segment1], c='r', lw=2)
plt.semilogy(alpha[segment2], steep[segment2], c='k', lw=2)
plt.semilogy(alpha[segment3], steep[segment3], c='k', lw=2)

plt.xlabel(r"$\alpha$")
plt.ylabel("empirical contraction rate")

plt.fill_between(alpha[segment1], 10*np.ones(alpha[segment1].shape), alpha=0.3, color='r')
plt.ylim((0.0001, 10*1))
plt.grid()
plt.xticks([0.0, 0.5, lower_bound_GC, 1.0, 1.5])

plt.show()  
