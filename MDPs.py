import torch


class mdp:

    def __init__(self, state_size, action_size, gamma, seed=0):

        self.state_size = state_size
        self.action_size = action_size
        self.seed = seed
        self.gamma = gamma

        torch.manual_seed(seed)

        #create the transition probability matrices sampling from a uniform distribution on the interval [0,1)
        self.P = torch.rand((self.action_size, self.state_size, self.state_size), dtype=torch.float64)
        #create the reward vectors sampling from a uniform distribution on the interval [0,1)
        self.r = torch.rand((self.action_size, self.state_size), dtype=torch.float64)

        #normalize the transition probability matrix
        for action in range(self.action_size):
            normalizing_factor = self.P[action, :, :].sum(dim=1)
            self.P[action, :, :] = self.P[action, :, :]/(torch.transpose(normalizing_factor.repeat(self.state_size,1), 0,1))

    def V_opt(self, seed=0, iterations=10**3):

        torch.manual_seed(seed)
        
        V = torch.randn((self.state_size, ), dtype=torch.float64)

        for iter in range(iterations):

            V = torch.max((self.r + self.gamma*torch.matmul(self.P, V)), 0)[0]

        return V



if __name__ == "__main__":

    mdp = mdp(5, 3, 0.9)